package socialNetwork;

import socialNetwork.domain.Friendship;
import socialNetwork.domain.Tuple;
import socialNetwork.domain.User;
import socialNetwork.domain.validation.FriendshipValidator;
import socialNetwork.domain.validation.UserValidator;
import socialNetwork.domain.validation.Validator;
import socialNetwork.repository.Repository;
import socialNetwork.repository.db.FriendshipDb;
import socialNetwork.repository.db.UserDb;
import socialNetwork.repository.fileRepository.FriendshipFileRepository;
import socialNetwork.repository.fileRepository.UserFileRepository;
import socialNetwork.service.FriendshipService;
import socialNetwork.service.UserService;
import socialNetwork.service.UserFrindshipsService;
import socialNetwork.ui.Console;

import java.io.FileNotFoundException;


public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        //repository
        String usersFileName = "data/users.csv";
        Validator<User> userValidator = new UserValidator();
        Repository<Long, User> userFileRepository = new UserFileRepository(usersFileName, userValidator);
        Repository<Long, User> userDbRepository = new UserDb("postgres", "postgres", "jdbc:postgresql://localhost:5432/social-network");

        String frienshipsFileName = "data/friendships.csv";
        Validator<Friendship> friendshipValidator = new FriendshipValidator();
        Repository<Tuple<Long, Long>, Friendship> friendshipFileRepository = new FriendshipFileRepository(
                frienshipsFileName, friendshipValidator);
        Repository<Tuple<Long, Long>, Friendship> friendshipDbRepo = new FriendshipDb("postgres", "postgres", "jdbc:postgresql://localhost:5432/social-network");




        //service
        UserService userService = new UserService(userDbRepository);
        FriendshipService friendshipService = new FriendshipService(friendshipDbRepo);
        UserFrindshipsService userFrindshipsService = new UserFrindshipsService(userService, friendshipService);


        //ui
        Console console = new Console(userService, friendshipService, userFrindshipsService);
        console.run();
    }
}


