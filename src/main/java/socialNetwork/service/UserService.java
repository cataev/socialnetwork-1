package socialNetwork.service;

import socialNetwork.domain.User;
import socialNetwork.repository.Repository;

public class UserService {
    private final Repository<Long, User> repo;

    /**
     * @param repo the repository with the an id and with users
     */
    public UserService(Repository<Long, User> repo) {
        this.repo = repo;
    }

    /**
     * @param user the user to add
     */
    public void addUser(User user) {
        repo.save(user);
    }

    /**
     * remove an user by id
     *
     * @param id the id of the user
     */
    public void removeUser(Long id) {
        repo.delete(id);
    }

    /**
     * @param user the new user to update
     */
    public void updateUser(User user) {
        repo.update(user);
    }

    /**
     * @param id the id of the entity to be returned id must not be null
     * @return the entity with the specified id or null - if there is no entity with the given id
     */
    public User findOne(Long id) {
        return repo.findOne(id);
    }

    /**
     * @return all the users
     */
    public Iterable<User> getAll() {
        return repo.findAll();
    }
}
