package socialNetwork.service;

public class UserFrindshipsService {
    private final UserService userService;
    private final FriendshipService friendshipService;

    /**
     * @param userService all the user's services
     * @param friendshipService all the user's friendship services
     */
    public UserFrindshipsService(UserService userService, FriendshipService friendshipService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
    }

    /**
     * remove the user with the given id and his frienship list
     *
     * @param id removed user's id
     */
    public void removeUserAndUserFriends(Long id){
        this.userService.removeUser(id);
        this.friendshipService.removeUserFriends(id);
    }
}
