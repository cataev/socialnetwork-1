package socialNetwork.service;

import socialNetwork.domain.Friendship;
import socialNetwork.domain.Tuple;
import socialNetwork.repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

public class FriendshipService {
    private final Repository<Tuple<Long, Long>, Friendship> repo;

    /**
     * @param repo the repository with the touple of id and a friendship
     */
    public FriendshipService(Repository<Tuple<Long, Long>, Friendship> repo) {
        this.repo = repo;
    }

    /**
     * @param friendship the frienship to add
     */
    public void addFriendship(Friendship friendship) {
        repo.save(friendship);
    }

    /**
     * @param id1 the id of the first user
     * @param id2 the id of the second user
     */
    public void removeFriendship(long id1, long id2) {
        this.repo.delete(new Tuple<>(id1, id2));
    }

    /**
     * @param friendship the frienship to update
     */
    public void updateFriendship(Friendship friendship) {
        repo.update(friendship);
    }

    /**
     * @return all the frienships
     */
    public Iterable<Friendship> getAll() {
        return repo.findAll();
    }


    public Friendship findOne(long id1, long id2){
        return this.repo.findOne(new Tuple<>(id1, id2));
    }

    /**
     * Delete all fiendships with the user with the given id
     *
     * @param id the id of the deleted user and his friends
     */
    public void removeUserFriends(Long id){
        List<Friendship> friendships = new ArrayList<>();
        for (Friendship friendship: this.repo.findAll()) {
            if(Objects.equals(friendship.getId().getLeft(), id) || Objects.equals(friendship.getId().getRight(), id)){
                friendships.add(friendship);
            }
        }
        for (Friendship friendship: friendships) {
            this.repo.delete(friendship.getId());
        }
    }

    /**
     * @return the number of conex components
     */
    public int numberConexComponents() {
        return dfs().getLeft();
    }

    /**
     * @return the list of friendships with the biggest number of conex components
     */
    public List<Friendship> theLongestRoad() {
        return dfs().getRight();
    }

    /**
     * @return a touple with an int that represents the number of conex components and a list with friendships withe the biggest number of conex components
     */
    private Tuple<Integer, List<Friendship>> dfs() {
        Iterable<Friendship> friendships = repo.findAll();
        System.out.println("Astea-s: " + friendships);
        boolean isEmpty = !friendships.iterator().hasNext();
        if (isEmpty) {
            return new Tuple<>(1, new ArrayList<>());
        }

        int numberOfConexComponents = 1;
        List<Friendship> visited = conexComponent(friendships.iterator().next(), friendships);
        List<Friendship> longestVisited = visited;

        List<Friendship> globalVisited = new ArrayList<>(visited);
        for (Friendship friendship : friendships) {
            if (!globalVisited.contains(friendship)) {
                visited = conexComponent(friendship, friendships);
                if (visited.size() > longestVisited.size()) {
                    longestVisited = visited;
                }
                globalVisited.addAll(visited);
                numberOfConexComponents++;
            }
        }
        return new Tuple<>(numberOfConexComponents, longestVisited);
    }

    /**
     * @param first the first friendship from where starts the search
     * @param friendships the list of all friendships
     * @return a list with conex components strarts from first
     */
    private List<Friendship> conexComponent(Friendship first, Iterable<Friendship> friendships) {
        Stack<Friendship> stack = new Stack<>();
        List<Friendship> visited = new ArrayList<>();
        stack.push(first);
        while (!stack.isEmpty()) {
            Friendship current = stack.pop();
            Long left = current.getId().getLeft();
            Long right = current.getId().getRight();
            if (!visited.contains(current)) {
                visited.add(current);
                for (Friendship friendship : friendships) {
                    Long left1 = friendship.getId().getLeft();
                    Long right1 = friendship.getId().getRight();
                    if (!visited.contains(friendship) && (Objects.equals(left, left1) || Objects.equals(left, right1) ||
                            Objects.equals(right, left1) || Objects.equals(right, right1))) {
                        stack.push(friendship);
                    }
                }
            }
        }
        return visited;
    }
}