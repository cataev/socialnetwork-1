package socialNetwork.domain;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Objects;


public class Friendship extends Entity<Tuple<Long, Long>> {

    Calendar calendar = Calendar.getInstance();

    /**
     * @param id1  the first id to set
     * @param id2  the second id to set
     * @param date the date when the friendship appeared
     */
    public Friendship(Long id1, Long id2, long date) {
        this.setId(new Tuple<>(id1, id2));
        calendar.setTimeInMillis(date);
    }


    /**
     * @return the date when the friendship was created
     */
    public long getDate() {
        return calendar.getTimeInMillis();

    }


    /**
     * @return the friendship in string format. It contains: id, date in dd.MM.yyyy-HH:mm:ss format.
     */
    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss");
        return "Friendship:{" +
                "id=" + getId() +
                ", date=" + Instant.ofEpochMilli(getDate()).atZone(ZoneId.systemDefault()).toLocalDate()/*formatter.format(calendar.getTime())*/ +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(calendar);
    }
}
