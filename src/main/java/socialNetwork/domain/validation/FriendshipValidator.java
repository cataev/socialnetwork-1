package socialNetwork.domain.validation;

import socialNetwork.domain.Friendship;
import socialNetwork.domain.Tuple;

import java.util.Objects;

public class FriendshipValidator implements Validator<Friendship> {

    @Override
    public void validate(Friendship entity) throws ValidationException {
        Tuple<Long, Long> id = entity.getId();
        if (Objects.equals(id.getLeft(), id.getRight())) {
            throw new ValidationException("Invalid Id");
        }
    }
}
