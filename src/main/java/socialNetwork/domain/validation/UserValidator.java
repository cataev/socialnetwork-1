package socialNetwork.domain.validation;

import socialNetwork.domain.User;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String firstName = entity.getFirstName().trim();
        if (firstName.isEmpty() || firstName.charAt(0) < 'A' || firstName.charAt(0) > 'Z') {
            throw new ValidationException("Invalid First Name");
        }

        String lastName = entity.getLastName().trim();
        if (lastName.isEmpty() || lastName.charAt(0) < 'A' || lastName.charAt(0) > 'Z') {
            throw new ValidationException("Invalid Last Name");
        }
    }
}
