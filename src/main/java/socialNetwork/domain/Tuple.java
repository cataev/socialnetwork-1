package socialNetwork.domain;

import java.util.Objects;


/**
 * Define a Tuple generic type entities
 * @param <E1> - tuple first entity type
 * @param <E2> - tuple second entity type
 */
public class Tuple<E1, E2> {
    private E1 e1;
    private E2 e2;

    /**
     * @param e1 the first entity of the touple
     * @param e2 the second entity of the touple
     */
    public Tuple(E1 e1, E2 e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    /**
     * @return the first entity of the touple
     */
    public E1 getLeft() {
        return e1;
    }

    /**
     * @param e1 set the first entity of the touple
     */
    public void setLeft(E1 e1) {
        this.e1 = e1;
    }

    /**
     * @return the second entity of the touple
     */
    public E2 getRight() {
        return e2;
    }

    /**
     * @param e2 set the secon entity of the touple
     */
    public void setRight(E2 e2) {
        this.e2 = e2;
    }

    /**
     * @return the touple in string format. It contains: first entity, last entity.
     */
    @Override
    public String toString() {
        return "" + e1 + "<->" + e2;
    }

    /**
     * @param obj An object that is verifying if is equal with the current user
     * @return true if the objects are equal and false othserwise
     */
    @Override
    public boolean equals(Object obj) {
        return this.e1.equals(((Tuple) obj).e1) && this.e2.equals(((Tuple) obj).e2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(e1, e2);
    }
}