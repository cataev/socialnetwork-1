package socialNetwork.ui;

import socialNetwork.domain.Friendship;
import socialNetwork.domain.User;
import socialNetwork.service.FriendshipService;
import socialNetwork.service.UserService;
import socialNetwork.service.UserFrindshipsService;

import java.security.KeyException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Scanner;

public class Console {
    private final UserService userService;
    private final FriendshipService friendshipService;
    private final UserFrindshipsService userFriendshipsService;

    public Console(UserService userService, FriendshipService friendshipService,
                   UserFrindshipsService userFriendshipsService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.userFriendshipsService = userFriendshipsService;
    }

    public void runUserOperations() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) Add user");
            System.out.println("(2) Delete user");
            System.out.println("(3) Update user");
            System.out.println("(4) Get all users");
            System.out.println("(5) Find by id");
            System.out.println("(0) Go back");

            System.out.println("Command:");
            String command = scanner.next();

            switch (command) {
                case "1":
                    try {
                        System.out.println("Id: ");
                        Long id = scanner.nextLong();
                        System.out.println("Frist name:");
                        String firstName = scanner.next();
                        System.out.println("Last name:");
                        String lastName = scanner.next();
                        User user = new User(firstName, lastName);
                        user.setId(id);
                        this.userService.addUser(user);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        userFriendshipsService.removeUserAndUserFriends(id);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "3":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        System.out.println("Frist name:");
                        String firstName = scanner.next();
                        System.out.println("Last name:");
                        String lastName = scanner.next();
                        User user = new User(firstName, lastName);
                        user.setId(id);
                        this.userService.updateUser(user);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "4":
                    try {
                        this.userService.getAll().forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "5":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        System.out.println(this.userService.findOne(id));
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public void runFriendshipOperations() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) Add frindship");
            System.out.println("(2) Delete frindship");
            System.out.println("(3) Update frindship");
            System.out.println("(4) Get all frindships");
            System.out.println("(5) Find a friendship by id");
            System.out.println("(6) Display number of comunities");
            System.out.println("(7) Display the most sociable comunity");
            System.out.println("(0) Back");

            System.out.println("Command:");
            String command = scanner.next();

            switch (command) {
                case "1":
                    try {
                        System.out.println("Id1:");
                        Long id1 = scanner.nextLong();
                        User user = userService.findOne(id1);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        System.out.println("Id2: ");
                        Long id2 = scanner.nextLong();
                        user = userService.findOne(id2);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        this.friendshipService.addFriendship(new Friendship(id1, id2, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    try {
                        System.out.println("Id1:");
                        long id1 = scanner.nextLong();
                        System.out.println("Id2: ");
                        long id2 = scanner.nextLong();
                        this.friendshipService.removeFriendship(id1, id2);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "3":
                    try {
                        System.out.println("Id1: ");
                        Long id1 = scanner.nextLong();
                        System.out.println("Id2: ");
                        Long id2 = scanner.nextLong();
                        this.friendshipService.updateFriendship(new Friendship(id1, id2, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "4":
                    try {
                        this.friendshipService.getAll().forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "5":
                    try {
                        System.out.println("Id1: ");
                        long id1 = scanner.nextLong();
                        System.out.println("Id2: ");
                        long id2 = scanner.nextLong();
                        System.out.println(this.friendshipService.findOne(id1, id2));
                    } catch (Exception e) {
                        System.out.println(e);
                        e.printStackTrace();

                    }
                    break;
                case "6":
                    try {
                        System.out.println("The number of conex components: " + friendshipService.numberConexComponents());
                    } catch (Exception e) {
                        System.out.println(e);
                        e.printStackTrace();

                    }
                    break;
                case "7":
                    try {
                        System.out.println("The most social community is: " + friendshipService.theLongestRoad());
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) User Operatinos");
            System.out.println("(2) Frienship Operation");
            System.out.println("(0) Exit");

            System.out.println("Command:");
            String command = scanner.next();

            switch (command) {
                case "1":
                    runUserOperations();
                    break;
                case "2":
                    runFriendshipOperations();
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }
}
