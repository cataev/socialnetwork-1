package socialNetwork.repository.fileRepository;

import socialNetwork.domain.Entity;
import socialNetwork.domain.validation.Validator;
import socialNetwork.repository.memory.InMemoryRepository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;


///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factori (vezi mai jos)
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {
    String fileName;

    /**
     * @param fileName the name of the file
     * @param validator entity's validator
     */
    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        loadFromFile();
    }

    /**
     * loads the data from file to memory
     */
    private void loadFromFile() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] args = line.split(";");
                E entity = extractEntity(Arrays.asList(args));
                super.save(entity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * writes an entity in file
     *
     * @param entity the entity that is written to file
     */
    protected void writeToFile(E entity) {
        String entityAsString = createEntityAsString(entity);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true))) {
            bufferedWriter.write(entityAsString);
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * rewrites an entity in file
     */
    protected void rewriteToFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(this.fileName, false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Iterable<E> entities = super.findAll();
        for (E e : entities) {
            String line = createEntityAsString(e) + "\n";
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.write(line.getBytes(StandardCharsets.UTF_8));
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        try {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * extract entity  - template method design pattern
     * creates an entity of type E having a specified list of @code attributes
     *
     * @param attributes
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);


    /**
     * @param entity
     * @return
     */
    protected abstract String createEntityAsString(E entity);

    /**
     * @param entity entity must be not null
     * @return entity
     */
    @Override
    public E save(E entity) {
        E result = super.save(entity);
        if (result == null) {
            writeToFile(entity);
        }
        return result;
    }

    /**
     * deletes the entity with the given id
     *
     * @param id id must be not null
     */
    @Override
    public void delete(ID id) {
        super.delete(id);
        rewriteToFile();
    }

    /**
     * updates an entity
     *
     * @param entity entity must not be null
     */
    @Override
    public void update(E entity) {
        super.update(entity);
        rewriteToFile();
    }
}

