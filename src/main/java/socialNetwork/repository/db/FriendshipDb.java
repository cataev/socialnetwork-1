package socialNetwork.repository.db;

import socialNetwork.domain.Friendship;
import socialNetwork.domain.Tuple;
import socialNetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class FriendshipDb implements Repository<Tuple<Long, Long>, Friendship> {

    private final String username;
    private final String password;
    private final String url;

    /**
     * @param username the username of the database admin
     * @param password the pasword of the database admin
     * @param url the url of the database
     */
    public FriendshipDb(String username, String password, String url) {
        this.username = username;
        this.password = password;
        this.url = url;
    }

    /**
     * @param friendshipId - the touple of id of the friendship
     * @return the frienship with the touple of id
     */
    @Override
    public Friendship findOne(Tuple<Long, Long> friendshipId) {
        if (friendshipId == null)
            throw new IllegalArgumentException("Id must be not null !");

        String sql = "SELECT * FROM friendship WHERE (first_user = (?) AND second_user = (?))" +
                " or (first_user = (?) AND second_user = (?))";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, friendshipId.getLeft());
            preparedStatement.setLong(2, friendshipId.getRight());
            preparedStatement.setLong(3, friendshipId.getRight());
            preparedStatement.setLong(4, friendshipId.getLeft());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                long id1 = resultSet.getLong("first_user");
                long id2 = resultSet.getLong("second_user");
                long date = resultSet.getLong("created_date");
                return new Friendship(id1, id2, date);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * @return all the friendships from the table
     */
    @Override
    public Iterable<Friendship> findAll() {

        String sql = "SELECT * from friendship";
        Set<Friendship> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long id1 = resultSet.getLong(1);
                long id2 = resultSet.getLong(2);
                long date = resultSet.getLong(3);

                Friendship friendship = new Friendship(id1, id2, date);
                friendships.add(friendship);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return friendships;
    }

    /**
     * @param entity entity must be not null 
     * @return the entity or null
     */
    @Override
    public Friendship save(Friendship entity) {
        String sql = "INSERT INTO friendship (first_user, second_user, created_date) VALUES (?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getId().getLeft());
            preparedStatement.setLong(2, entity.getId().getRight());
            preparedStatement.setLong(3, entity.getDate());

            preparedStatement.execute();

            return entity;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * Delete a frienship with the given tuple of id
     * 
     * @param friendshipId - the given tuple of id
     */
    @Override
    public void delete(Tuple<Long, Long> friendshipId) {
        String sql = "DELETE FROM friendship WHERE (first_user = (?) AND second_user = (?))" +
                "or (first_user = (?) AND second_user = (?))";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, friendshipId.getLeft());
            preparedStatement.setLong(2, friendshipId.getRight());
            preparedStatement.setLong(3, friendshipId.getRight());
            preparedStatement.setLong(4, friendshipId.getLeft());

            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Update a friendship with the given touple of id and modify the date
     * 
     * @param entity entity must not be null 
     */
    @Override
    public void update(Friendship entity) {
        String sql = "UPDATE friendship SET created_date = (?) WHERE (first_user = (?) AND second_user = (?))";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getDate());
            preparedStatement.setLong(2, entity.getId().getLeft());
            preparedStatement.setLong(3, entity.getId().getRight());

            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
