package socialNetwork.repository.memory;

import socialNetwork.domain.Entity;
import socialNetwork.domain.validation.Validator;
import socialNetwork.repository.Repository;

import java.util.HashMap;
import java.util.Map;


/**
 *  Define a InMemoryRepository generic type entities
 * @param <ID> - inMemoryRepository id type
 * @param <E> - inMemoryRepository entity type
 */
public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {

    private Validator<E> validator;
    Map<ID, E> entities;

    /**
     * @param validator the validator of entities
     */
    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<ID, E>();
    }

    /**
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return
     */
    @Override
    public E findOne(ID id) {
        if (id == null)
            throw new IllegalArgumentException("Id must be not null !");
        return entities.get(id);
    }

    /**
     * @return all entities
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     * @param entity entity must be not null
     * @return the entity
     */
    @Override
    public E save(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null !");
        validator.validate(entity);
        if (entities.get(entity.getId()) != null) {
            throw new IllegalArgumentException("Entity with given id already exists !");
        } else entities.put(entity.getId(), entity);
        return null;
    }

    /**
     * @param id id must be not null
     */
    @Override
    public void delete(ID id) {
        if (entities.get(id) == null) throw new IllegalArgumentException("entity with given id dose not exist");
        entities.remove(id);
    }

    /**
     * Updates an entity
     *
     * @param entity entity must not be null
     */
    @Override
    public void update(E entity) {

        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null !");
        validator.validate(entity);

        entities.put(entity.getId(), entity);

        if (entities.get(entity.getId()) != null) {
            entities.put(entity.getId(), entity);
        }
    }
}
